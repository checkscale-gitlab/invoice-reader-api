FROM ubuntu:20.04

LABEL maintainer="Michell Algarra"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -y
RUN apt install tesseract-ocr -y \
  python3 \
  python3-pip \
  python3-opencv

WORKDIR /invoice-reader-api

COPY ./requirements.txt /invoice-reader-api/requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

EXPOSE 3000

ENTRYPOINT [ "python3" ]
CMD [ "wsgi.py" ]
